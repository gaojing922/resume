module.exports = {
  name: '高景', // 姓名
  phone: '18573023008', // 手机号
  email: '18573023008@163.com', // email
  wechat: '18573023008', // 微信号
  workingState: '在职', // 在职状态
  workExperience: '5年', // 工作经验
  education: '大专', // 学历
  native: '湖南', // 籍贯
  currentAddress: '深圳', // 现居
  position: 'web前端', // 求职岗位
  salary: '15000-20000元/月', // 薪资要求
  workCity: '深圳', // 工作城市
  jobNature: '全职', // 工作性质
  // 掌握技能
  skillList: [
    {name: 'html5', value: '95'},
    {name: 'css3', value: '95'},
    {name: 'js', value: '95'},
    {name: 'es6/7/8', value: '95'},
    {name: 'typescript', value: '90'},
    {name: 'vue', value: '95'},
    {name: 'react', value: '95'},
    {name: 'angular', value: '80'},
    {name: 'element/ivew/antd', value: '95'},
    {name: 'webpack', value: '90'},
    {name: '微信小程序', value: '80'},
    {name: 'react-native', value: '80'},
    {name: 'nodejs', value: '90'},
    {name: 'koa/express/egg', value: '90'}
  ],
  // 工作经历
  jobList: [
    { company: '深圳市大浩进出口贸易有限公司', time: '2019.01至今'},
    {company: '深圳市古瑞瓦特新能源古份有限公司', time: '2016-05至 2019-01'},
    {company: '深度网深圳分公司', time: '2015-05至 2016-05'}
  ],
  // 项目经验
  projectList: [
    {title: '商城H5', name: 1, children: [
      {
        technical: 'react全家桶(react-router,redux, react-redux, redux-thunk, hooks),axios, antd, html，less, js, es6, webpack4',
        address: 'http://47.113.118.17:1991 (*自己的项目*)',
        jobZz: [
          '1. 使用webpack4.0手动搭建react开发环境,配置各种loader,plugins,使用redux进行调试',
          '2. 使用axios封装http网络请求(get跟post)',
          '3. UI框架使用antd,配置按需加载',
          '4. 使用html,less实现布局，使用es6进行业务层代码编写',
          '5. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '6. 使用redux结合react-redux实现数据状态管理，中间件使用redux-thunk,实现action中的异步操作',
          '7. 整个项目使用hooks进行组件编写(useState, useEffect, useMemo, memo, useCallback, useRef)',
          '8. 后台使用nodejs + egg + mongodb进行开发， 登录鉴权使用session'
        ]
      }
    ]},
    {title: '资金管理系统', name: 2, children: [
      {
        technical: 'vue全家桶(vue-router,vuex), axios, elementUI, html，less, js, es6, webpack4',
        address: 'http://47.113.118.17:1992 (*自己的项目*)',
        jobZz: [
          '1. 使用webpack4.0手动搭建vue开发环境,配置各种loader,plugins',
          '2. 使用axios封装http网络请求(get跟post)',
          '3. UI框架使用elementUI',
          '4. 使用html,less实现布局，使用es6进行业务层代码编写',
          '5. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '6. 使用echarts以图表形式展示数据',
          '7. 后台使用nodejs + koa2 + mongodb进行开发， 登录鉴权使用jwt,实现简单的数据增删改查'
        ]
      }
    ]},
    {title: 'eprolo pod', name: 3, children: [
      {
        technical: 'vue全家桶(vue-router, vuex), ivew',
        address: 'https://pod.eprolo.com/',
        jobZz: [
          '1. 使用webpack4.0手动搭建vue开发环境,配置各种loader,plugins,使用redux进行调试',
          '2. 使用axios封装http网络请求(get跟post)',
          '3. UI框架使用ivew,配置按需加载',
          '4. 使用html,less实现布局，使用es6进行业务层代码编写',
          '5. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '6. 负责项目上线发布'
        ]
      }
    ]},
    {title: '◆eprolo后台管理系统', name: 4, children: [
      {
        technical: 'react全家桶(react-router,redux, react-redux, redux-thunk), antd',
        address: '内部使用',
        jobZz: [
          '1. 使用webpack4.0手动搭建react开发环境,配置各种loader,plugins',
          '2. 使用axios封装http网络请求',
          '3. UI框架使用antd',
          '4. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '5. 负责线上打包配置'
        ]
      }
    ]},
    {title: '深圳古瑞瓦特新能源客服管理系统', name: 6, children: [
      {
        technical: 'html,css3,js,es6,vue全家桶(vue-router, vuex)，ivew, axios, highcharts,谷歌地图',
        address: 'oss.growatt.com',
        jobZz: [
          '1. 使用vue-cli脚手架创建项目，基于些项目上进行一些配置',
          '2. 使用axios封装http网络请求(get跟post)',
          '3. UI框架使用ivew，对常用组件进行二次封装(table)',
          '4. 使用html,less实现布局，使用es6进行业务层代码编写',
          '5. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '6. 使用highcharts以图表形式展示数据，使用谷歌地图进行定位',
          '7. 负责项目上线发布'
        ]
      }
    ]},
    {title: '深圳古瑞瓦特新能源电站监控系统', name: 7, children: [
      {
        technical: 'react全家桶(react-router,redux, react-redux, redux-thunk),axios, antd, html，less, js, es6，echarts',
        address: 'server.growatt.com',
        jobZz: [
          '1. 使用webpack4.0手动搭建react开发环境,配置各种loader,plugins,使用redux进行调试',
          '2. 使用axios封装http网络请求(get跟post)',
          '3. UI框架使用antd,配置按需加载',
          '4. 使用html,less实现布局，使用es6进行业务层代码编写',
          '5. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '6. 使用redux结合react-redux实现数据状态管理，中间件使用redux-thunk,实现action中的异步操作',
          '7. 负责项目上线发布'
        ]
      }
    ]},
    {title: '深圳古瑞瓦特新能智能物联网', name: 8, children: [
      {
        technical: 'react全家桶(react-router,redux, react-redux, redux-thunk),axios, antd, html，less, js, es6，echarts',
        address: 'oss.energy.com',
        jobZz: [
          '1. 使用webpack4.0手动搭建react开发环境,配置各种loader,plugins,使用redux进行调试',
          '2. 使用axios封装http网络请求(get跟post)',
          '3. UI框架使用antd,配置按需加载',
          '4. 使用html,less实现布局，使用es6进行业务层代码编写',
          '5. 对项目进行性能优化，包括项目打包体积大小(压缩css, js, 图片，消除未使用的css,js代码，CDN加载)，运行性能优化(css,js分离，按需加载)',
          '6. 使用redux结合react-redux实现数据状态管理，中间件使用redux-thunk,实现action中的异步操作',
          '7. 负责项目上线发布'
        ]
      }
    ]},
    {title: '通商网(购物网站)', name: 9, children: [
      {
        technical: 'html,css3,js,jquery,zepto',
        address: 'www.ts5000.com',
        jobZz: [
          '1. 使用html,css进行页面常规布局，使用bootstrap实现响应式布局',
          '2. 使用jquery实现业务编写,ajax与后端进行交互'
        ]
      }
    ]}
  ],
  // 自我评价
  selfAssessment: [
    '抗压能力强，勇于面对困难，坚持达成预定目标',
    '责任心强，注重团队合作',
    '性格偏外向，踏实，几年的工作经历，变得更加成熟，稳重',
    '希望能在一个有着激情，有活力，有想法的团队中成长',
    '执着，认准的事一定要做好，执行力强'
  ]
}