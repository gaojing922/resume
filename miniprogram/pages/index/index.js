const config = require('../../datas/config')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    config,
    showBox: false,
    activeNames: [],
    animation: '',
    classArry: ['two', 'three', 'four'],
    visible_0: false,
    visible_1: false,
    visible_2: false,
    isReloadScroll: true
  },

  onChange (event) {
    this.setData({
      activeNames: event.detail
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  start () {
    let animation = wx.createAnimation({
      duration: 500,
      timingFunction: 'linear',
      delay: 100
    })
    animation.opacity(1).translateX(0).step()
    this.setData({
      animation:  animation.export()
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.start()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 判断元素是否在可视区域内
  initTabObserver (dom, tabFixed) {
    if (this.data.visible_2) {
      this.setData({
        isReloadScroll: false
      })
    }
    this.tabObserver = wx.createIntersectionObserver(this)
    this.tabObserver.relativeToViewport().observe(dom, res => {
      const visible = res.intersectionRatio > 0
      this.setData({
        [tabFixed]: visible
      })
    })
  },
  onPageScroll (e) {
    if (!this.data.isReloadScroll) {
      return
    }
    this.data.classArry.forEach((item, index) => {
      this.initTabObserver(`.${item}`, `visible_${index}`)
    })
  }
})